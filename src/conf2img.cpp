#include "image_ppm.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

int main(int argc, char** argv)
{
	if(argc != 5)
	{
		std::ostringstream ossErr;
		
		ossErr << "Utilisation : " << argv[0] << " fichier.conf nH nW g/c";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	char* fichierConf = argv[1];
	int nH = atoi(argv[2]);
	int nW = atoi(argv[3]);
	char grisOuCouleur = argv[4][0];
	
	int nTaille = (nH * nW);
	
	if(grisOuCouleur == 'c') // Sinon, si c'est une image en niveau de couleurs, alors...
	{
		nTaille *= 3;
	}
	
	OCTET *img;
	
	allocation_tableau(img, OCTET, nTaille);
	
	std::ifstream ifs;
	
	ifs.open(fichierConf);
	
	if(!ifs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Impossible d'ouvrir le fichier \"" << fichierConf << "\" en mode lecture.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	std::string pixel;
	size_t i = 0;
	size_t j = 0;
	OCTET r(0), v(0);
	
	while(ifs >> pixel)
	{
		OCTET tmp = (OCTET)stoi(pixel);
		
		if(grisOuCouleur == 'g') // Si c'est une image en niveau de gris, alors...
		{
			img[i] = tmp;
		}
		else if(grisOuCouleur == 'c') // Sinon, si c'est une image en niveau de couleurs, alors...
		{
			if((i % 3) == 0)
				r = tmp;
			else if((i % 3) == 1)
				v = tmp;
			else if((i % 3) == 2)
			{
				img[3*j] = r;
				img[3*j+1] = v;
				img[3*j+2] = tmp;
			}
			
			if((i % 3) == 2)
				++j;
		}
		++i;
	}
	
	if(grisOuCouleur == 'c') // Si c'est une image en niveau de couleurs, alors...
	{
		for (int i=0;i<nH;++i)
		{
			for (int j=0;j<nW;++j)
				std::cout << (int)img[3 * (i*nW+j)] << " " << (int)img[3 * (i*nW+j) + 1] << " " << (int)img[3 * (i*nW+j) + 2] << " ";
			
			std::cout << std::endl;
		}
	}
	
	std::ostringstream oss;
	
	oss << fichierConf;
	
	if(grisOuCouleur == 'g') // Si c'est une image en niveau de gris, alors...
	{
		oss << ".pgm";
		
		ecrire_image_pgm(oss.str().c_str(), img, nH, nW);
	}
	else if(grisOuCouleur == 'c') // Sinon, si c'est une image en niveau de couleurs, alors...
	{
		oss << ".ppm";
		
		ecrire_image_ppm(oss.str().c_str(), img, nH, nW);
	}
	
	free(img);
	
	return 0;
}
