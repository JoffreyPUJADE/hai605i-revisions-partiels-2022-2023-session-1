#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "image_ppm.h"

// Fonction réalisées pour le TP 1.

static inline void seuillage1Seuil(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int S)
{
	for (int i=0;i<nH;++i)
	{
		for (int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < S)
				imgOut[i*nW+j] = 0;
			else
				imgOut[i*nW+j] = 255;
		}
	}
}

static inline void seuillage2Seuils(OCTET* imgIn, OCTET* imgOut, int nH, int nW, int S1, int S2)
{
	for (int i=0;i<nH;++i)
	{
		for (int j=0;j<nW;++j)
		{
			if(imgIn[i*nW+j] < S1)
				imgOut[i*nW+j] = 0;
			else if(imgIn[i*nW+j] < S2)
				imgOut[i*nW+j] = 128;
			else
				imgOut[i*nW+j] = 255;
		}
	}
}

#endif // TRANSFORMATIONS_HPP
